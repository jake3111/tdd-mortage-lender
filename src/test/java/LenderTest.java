import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;
import java.util.Date;
import java.util.Calendar;


final class LenderTest {

    @Test public void testLenderGetAvailableFundsFail() {
        Lender lender = new Lender(500);
        assertNotEquals(lender.getAvailableFunds(), 600);
    }

    @Test public void testLenderGetAvailableFundsPass() {
        Lender lender = new Lender(500);
        assertEquals(lender.getAvailableFunds(), 500);
    }

    @Test public void testLenderAddFundsFail() {
        Lender lender = new Lender(500);
        lender.addFunds(100);
        assertNotEquals(lender.getAvailableFunds(), 500);
    }

    @Test public void testLenderAddFundsPass() {
        Lender lender = new Lender(500);
        lender.addFunds(100);
        assertEquals(lender.getAvailableFunds(), 600);
    }

    @Test public void testLenderisApplicantApproved() {
        Lender lender = new Lender(1000);
        assertEquals(lender.isApplicantApproved(900), true);
    }

    @Test public void testLenderDenyApplicant() {
        Lender lender = new Lender(1000);
        assertEquals(lender.isApplicantApproved(2000), false);
    }

    @Test public void testLenderCalculateMortagePass() {
        Lender lender = new Lender(6000);
        assertEquals(lender.calculateMortagePayment(60000, .03, 120), 579);
    }

    @Test public void testLenderCalculateMortageFail() {
        Lender lender = new Lender(6000);
        assertNotEquals(lender.calculateMortagePayment(60000, .03, 120), 200);
    }

    @Test public void doesLenderQualifyPass() {
        Lender lender = new Lender(6000);
        assertTrue(lender.doesQualify(.3, 700, 15000, 60000));
    }

    @Test public void doesLenderQualifyFail() {
        Lender lender = new Lender(6000);
        assertFalse(lender.doesQualify(.5, 700, 19000, 60000));
    }

    @Test public void shouldOfferLoanFail() {
        Lender lender = new Lender(900000000);
        assertTrue(lender.doesQualify(.3, 700, 15000, 60000));
    }

    @Test public void shouldOfferLoanPass() {
        Lender lender = new Lender(6000);
        assertFalse(lender.doesQualify(.5, 700, 19000, 60000));
    }

    @Test public void offerLoanPass() {
        Lender lender = new Lender(900000000);
        assertTrue(lender.offerLoan(.3, 700, 15000, 60000, true));
    }

    @Test public void offerLoanFail() {
        Lender lender = new Lender(900000000);
        assertFalse(lender.offerLoan(.3, 700, 15000, 60000, false));
    }

    @Test public void sendLoanAmountPass() {
        Lender lender = new Lender(900000000);
        assertEquals(lender.sendLoanAmount(.3, 700, 15000, 60000, true), 60000);
        assertEquals(lender.getSecondAccount(), 899940000);
    }

    @Test public void sendLoanAmountFail() {
        Lender lender = new Lender(900000000);
        lender.sendLoanAmount(.3, 700, 15000, 60000, false);
        assertEquals(lender.getSecondAccount(), 0);
    }

    @Test public void sendLoanAmountThenReclaimFunds() {
        Lender lender = new Lender(900000000);
        assertEquals(lender.sendLoanAmount(.3, 700, 15000, 60000, true), 60000);
        assertEquals(lender.getSecondAccount(), 899940000);
        lender.reclaimFunds();
        assertEquals(lender.getAvailableFunds(), 1799940000);
    }

    @Test public void giveLoanContractPass() {
        Lender lender = new Lender(900000000);
        LoanContract loanContract = lender.giveLoanContract(.3, 700, 15000, 60000, true);

        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, 3);  // number of days to add
        Date date = c.getTime();  // dt is now the new date
        assertEquals(date.getDay(), loanContract.getLoanExpirationDate().getDay());
        assertEquals(date.getMonth(), loanContract.getLoanExpirationDate().getMonth());
        assertEquals(date.getYear(), loanContract.getLoanExpirationDate().getYear());
        assertEquals(60000, loanContract.getLoanAmount());
    }

    @Test public void giveLoanContractFail() {
        Lender lender = new Lender(900000000);
        LoanContract loanContract = lender.giveLoanContract(.3, 700, 15000, 60000, false);
        assertNull(loanContract);
    }

    @Test public void sendLoanAmountFail() {
        Lender lender = new Lender(900000000);
        lender.sendLoanAmount(.3, 700, 15000, 60000, false);
        assertEquals(lender.getSecondAccount(), 0);
    }

    @Test public void sendLoanAmountThenReclaimFunds() {
        Lender lender = new Lender(900000000);
        assertEquals(lender.sendLoanAmount(.3, 700, 15000, 60000, true), 60000);
        assertEquals(lender.getSecondAccount(), 899940000);
        lender.reclaimFunds();
        assertEquals(lender.getAvailableFunds(), 1799940000);
    }

    @Test public void giveLoanContractPass() {
        Lender lender = new Lender(900000000);
        LoanContract loanContract = lender.giveLoanContract(.3, 700, 15000, 60000, true);

        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, 3);  // number of days to add
        Date date = c.getTime();
        assertEquals(date.getDay(), loanContract.getLoanExpirationDate().getDay());
        assertEquals(date.getMonth(), loanContract.getLoanExpirationDate().getMonth());
        assertEquals(date.getYear(), loanContract.getLoanExpirationDate().getYear());
        assertEquals(60000, loanContract.getLoanAmount());
    }

    @Test public void giveLoanContractFail() {
        Lender lender = new Lender(900000000);
        LoanContract loanContract = lender.giveLoanContract(.3, 700, 15000, 60000, false);
        assertNull(loanContract);
    }
}
