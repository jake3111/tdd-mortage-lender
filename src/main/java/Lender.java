import java.util.Date;
import java.util.Calendar;

final class Lender {
    private long availableFunds;
    private long secondAccount;


    public Lender(long availableFunds) {
        this.availableFunds = availableFunds;
        this.secondAccount = 0;
    }

    public long getSecondAccount() {
        return secondAccount;
    }

    public long getAvailableFunds() {
        return availableFunds;
    }

    public void addFunds(long extraFunds) {
        availableFunds += extraFunds;
    }

    public boolean isApplicantApproved(long requestedFunds) {
        return availableFunds >= requestedFunds;
    }

    public int calculateMortagePayment(long principal, double interestRate, int numberOfPayments){
        double monthlyPayment = (principal * (interestRate/12) * Math.pow((1+(interestRate/12)),numberOfPayments)) / ((Math.pow((1 + (interestRate/12)),numberOfPayments) - 1));
        return (int)monthlyPayment;
    }

    public boolean doesQualify(double debtToIncome, int creditScore, long amountSaved, long loanAmount){
        return (debtToIncome < .36 && creditScore > 620 && amountSaved >= (.25 * loanAmount));
    }

    public boolean offerLoan(double debtToIncome, int creditScore, long amountSaved, long loanAmount) {
        return isApplicantApproved(loanAmount) && doesQualify(debtToIncome, creditScore, amountSaved, loanAmount);
    }

    public boolean offerLoan(double debtToIncome, int creditScore, long amountSaved, long loanAmount, boolean acceptsLoan) {
        return isApplicantApproved(loanAmount) && doesQualify(debtToIncome, creditScore, amountSaved, loanAmount) && acceptsLoan;
    }

    public long sendLoanAmount(double debtToIncome, int creditScore, long amountSaved, long loanAmount, boolean acceptsLoan) {
        long loanAmountToSend = (isApplicantApproved(loanAmount) && doesQualify(debtToIncome, creditScore, amountSaved, loanAmount) && acceptsLoan) ? loanAmount: 0;
        if (loanAmountToSend != 0) {
            secondAccount = availableFunds - loanAmountToSend;
        }
        return loanAmountToSend;
    }

    public void reclaimFunds() {
        availableFunds += secondAccount;
    }

    public LoanContract giveLoanContract(double debtToIncome, int creditScore, long amountSaved, long loanAmount, boolean acceptsLoan) {
        long loanAmountFromOffer = sendLoanAmount(debtToIncome, creditScore, amountSaved, loanAmount, acceptsLoan);
        if (loanAmountFromOffer != 0) {
            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            c.add(Calendar.DATE, 3);  // number of days to add
            Date date = c.getTime();
            return new LoanContract(loanAmountFromOffer, date);
        }
        return null;
    }
}
