import java.util.Date;

final class LoanContract {
    private long loanAmount;

    private Date loanExpirationDate;

    public LoanContract(long loanAmount, Date loanExpirationDate) {
        this.loanAmount = loanAmount;
        this.loanExpirationDate = loanExpirationDate;
    }

    public long getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(long loanAmount) {
        this.loanAmount = loanAmount;
    }

    public Date getLoanExpirationDate() {
        return loanExpirationDate;
    }

    public void setLoanExpirationDate(Date loanExpirationDate) {
        this.loanExpirationDate = loanExpirationDate;
    }
}